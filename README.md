# Docker Magento2 CI

Set of docker images used for CI

1. Put to `docker/magento2/auth.json` a file containing your credentials to `repo.magento.com`. [More info](https://devdocs.magento.com/cloud/setup/first-time-setup-import-prepare.html#auth-json)
2. Run `bin/prepare-ci.sh <MAGENTO_VERSION>` with magento version predefined for `docker-compose.base.<MAGENTO_VERSION>.yaml` file

