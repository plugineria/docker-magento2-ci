#!/usr/bin/env bash

set -e

MAGENTO_VERSION=${1:-"2.3.6"}
DOCKER_COMPOSE_BASE_PATH="docker-compose.base.yaml"
DOCKER_COMPOSE_BASE_VERSION_PATH="docker-compose.base.${MAGENTO_VERSION}.yaml"

echo "== Using docker compose for file: $DOCKER_COMPOSE_BASE_VERSION_PATH =="

docker-compose -f "$DOCKER_COMPOSE_BASE_PATH" -f "$DOCKER_COMPOSE_BASE_VERSION_PATH" down -v

echo "== Build base container for magento $MAGENTO_VERSION =="

docker-compose -f "$DOCKER_COMPOSE_BASE_PATH" -f "$DOCKER_COMPOSE_BASE_VERSION_PATH" build --no-cache --force-rm --pull

echo "== Run base application =="

docker-compose -f "$DOCKER_COMPOSE_BASE_PATH" -f "$DOCKER_COMPOSE_BASE_VERSION_PATH" up -d

echo "== Wait until mysql starts properly =="

sleep 120

echo "== Install magento with sample data =="

docker-compose -f "$DOCKER_COMPOSE_BASE_PATH" -f "$DOCKER_COMPOSE_BASE_VERSION_PATH" exec magento2_base install-magento
docker-compose -f "$DOCKER_COMPOSE_BASE_PATH" -f "$DOCKER_COMPOSE_BASE_VERSION_PATH" exec magento2_base install-sampledata

echo "== Pull generated composer.json from container =="

mkdir -p "docker/magento2-ci/data/${MAGENTO_VERSION}"
BASE_CONTAINER_ID=$(docker ps -aqf "name=magento2_base")
docker cp $BASE_CONTAINER_ID:/var/www/html/composer.json "docker/magento2-ci/data/${MAGENTO_VERSION}/composer.json"

echo "== Clean up containers =="

docker-compose -f "$DOCKER_COMPOSE_BASE_PATH" -f "$DOCKER_COMPOSE_BASE_VERSION_PATH" down -v

echo "== Build final image for ${MAGENTO_VERSION} =="

docker-compose build --force-rm --no-cache "magento2_ci_${MAGENTO_VERSION//\./_}"

echo "== Push to docker registry ${MAGENTO_VERSION} =="

docker-compose push "magento2_ci_${MAGENTO_VERSION//\./_}"
